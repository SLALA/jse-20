package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(final IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null) throw new AccessDeniedException();
        return entityRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null) throw new AccessDeniedException();
        if (comparator == null) return null;
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null) throw new AccessDeniedException();
        entityRepository.clear(userId);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = entityRepository.findById(userId, id);
        return entity;
    }

    @Override
    public E findByName(final String userId, final String name) {
        if (userId == null) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = entityRepository.findByName(userId, name);
        return entity;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (userId == null) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        final E entity = entityRepository.findByIndex(userId, index);
        return entity;
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        E entity = findById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        entity = entityRepository.removeById(userId, id);
        return entity;
    }

    @Override
    public E removeByName(final String userId, final String name) {
        if (userId == null) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = entityRepository.removeByName(userId, name);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        if (userId == null) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        final E entity = entityRepository.removeByIndex(userId, index);
        return entity;
    }

    @Override
    public E updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateByName(final String userId, final String oldName, final String name, final String description) {
        if (userId == null) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByName(userId, oldName);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null) throw new AccessDeniedException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null) throw new AccessDeniedException();
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null) throw new AccessDeniedException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

}
