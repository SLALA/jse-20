package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;
import ru.t1.strelcov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository repository;

    public ProjectService(final IProjectRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (userId == null) throw new AccessDeniedException();
        final Project project;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty())
            project = new Project(userId, name);
        else
            project = new Project(userId, name, description);
        add(project);
        return project;
    }

}
