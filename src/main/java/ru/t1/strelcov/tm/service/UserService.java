package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.*;
import ru.t1.strelcov.tm.exception.entity.*;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User add(final String login, final String password) {
        final User user;
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (findByLogin(login) != null) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @Override
    public User add(final String login, final String password, final String email) {
        final User user;
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (findByLogin(login) != null) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @Override
    public User add(final String login, final String password, final Role role) {
        final User user;
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (findByLogin(login) != null) throw new UserLoginExistsException();
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user = userRepository.removeByLogin(login);
        return user;
    }

    @Override
    public User updateById(final String id, final String firstName, final String lastName, final String middleName, final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateByLogin(final String login, final String firstName, final String lastName, final String middleName, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public void changePasswordById(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        final String passwordHash = HashUtil.salt(password);
        user.setPasswordHash(passwordHash);
    }

}
