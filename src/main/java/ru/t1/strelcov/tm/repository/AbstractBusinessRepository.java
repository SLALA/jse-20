package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()))
                entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()))
                entities.add(entity);
        }
        entities.sort(comparator);
        return entities;
    }

    @Override
    public void clear(final String userId) {
        final List<E> entities = findAll(userId);
        list.removeAll(entities);
    }

    @Override
    public E findByName(final String userId, final String name) {
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()) && name.equals(entity.getName()))
                return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entities = findAll(userId);
        if (index >= entities.size()) return null;
        return entities.get(index);
    }

    @Override
    public E findById(final String userId, final String id) {
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                return entity;
        }
        return null;
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByName(final String userId, final String name) {
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

}
