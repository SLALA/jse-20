package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findByName(String userId, String name);

    E findByIndex(String userId, Integer index);

    E removeByName(String userId, String name);

    E removeByIndex(String userId, Integer index);

    E findById(String userId, String id);

    E removeById(String userId, String id);

    void clear(String userId);

}
