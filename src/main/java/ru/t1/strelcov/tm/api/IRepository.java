package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void add(E user);

    void clear();

    void remove(E user);

    E findById(String id);

    E removeById(String id);

}
