package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
