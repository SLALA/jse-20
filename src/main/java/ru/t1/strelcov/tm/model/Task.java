package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public final class Task extends AbstractBusinessEntity {

    private String projectId;

    public Task() {
        super();
    }

    public Task(String userId, String name) {
        super(userId, name);
    }

    public Task(String userId, String name, String description) {
        super(userId, name, description);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
